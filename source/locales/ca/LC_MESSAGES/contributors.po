# Catalan translation of the Debian Release Notes
#
# Miguel Gea Milvaques, 2006-2009.
# Jordà Polo, 2007, 2009.
# Guillem Jover <guillem@debian.org>, 2007, 2019.
# Héctor Orón Martínez, 2011.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2024-01-15 18:03+0100\n"
"PO-Revision-Date: 2019-07-19 01:07+0200\n"
"Last-Translator: Alex Muntada <alexm@debian.org>\n"
"Language: ca\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../contributors.rst:4
msgid "Contributors to the Release Notes"
msgstr "Contribuïdors de les notes de llançament"

#: ../contributors.rst:6
msgid "Many people helped with the release notes, including, but not limited to"
msgstr ""
"Molta gent ha ajudat en les notes de llançament, estan incloses, però no "
"limitades a"

#: ../contributors.rst:8
msgid ":abbr:`Adam D. Barrat (various fixes in 2013)`,"
msgstr ""

#: ../contributors.rst:9
msgid ":abbr:`Adam Di Carlo (previous releases)`,"
msgstr ":abbr:`Adam Di Carlo (llançaments anteriors)`,"

#: ../contributors.rst:10
msgid ":abbr:`Andreas Barth aba (previous releases: 2005 - 2007)`,"
msgstr ":abbr:`Andreas Barth aba (llançaments anteriors: 2005 - 2007)`,"

#: ../contributors.rst:11
msgid ":abbr:`Andrei Popescu (various contributions)`,"
msgstr ":abbr:`Andrei Popescu (contribucions diverses)`,"

#: ../contributors.rst:12
msgid ":abbr:`Anne Bezemer (previous release)`,"
msgstr ":abbr:`Anne Bezemer (llançament anterior)`,"

#: ../contributors.rst:13
msgid ":abbr:`Bob Hilliard (previous release)`,"
msgstr ":abbr:`Bob Hilliard (llançament anterior)`,"

#: ../contributors.rst:14
msgid ":abbr:`Charles Plessy (description of GM965 issue)`,"
msgstr ":abbr:`Charles Plessy (descripció del problema GM965)`,"

#: ../contributors.rst:15
msgid ":abbr:`Christian Perrier bubulle (Lenny installation)`,"
msgstr ":abbr:`Christian Perrier bubulle (instal·lació de Lenny)`,"

#: ../contributors.rst:16
msgid ":abbr:`Christoph Berg (PostgreSQL-specific issues)`,"
msgstr ":abbr:`Christoph Berg (problemes especifics de PostgreSQL)`,"

#: ../contributors.rst:17
msgid ":abbr:`Daniel Baumann (Debian Live)`,"
msgstr ""

#: ../contributors.rst:18
msgid ":abbr:`David Prévot taffit (Wheezy release)`,"
msgstr ":abbr:`David Prévot taffit (llançament de Wheezy)`,"

#: ../contributors.rst:19
msgid ":abbr:`Eddy Petrișor (various contributions)`,"
msgstr ":abbr:`Eddy Petrișor (contribucions diverses)`,"

#: ../contributors.rst:20
msgid ":abbr:`Emmanuel Kasper (backports)`,"
msgstr ""

#: ../contributors.rst:21
msgid ":abbr:`Esko Arajärvi (rework X11 upgrade)`,"
msgstr ":abbr:`Esko Arajärvi (reescriptura de l'actualització de les X11)`,"

#: ../contributors.rst:22
msgid ":abbr:`Frans Pop fjp (previous release Etch)`,"
msgstr ":abbr:`Frans Pop fjp (llançament anterior Etch)`,"

#: ../contributors.rst:23
msgid ":abbr:`Giovanni Rapagnani (innumerable contributions)`,"
msgstr ":abbr:`Giovanni Rapagnani (moltíssimes contribucions)`,"

#: ../contributors.rst:24
msgid ":abbr:`Gordon Farquharson (ARM port issues)`,"
msgstr ":abbr:`Gordon Farquharson (problemes del port d'ARM)`,"

#: ../contributors.rst:25
msgid ":abbr:`Hideki Yamane henrich (contributed and contributing since 2006)`,"
msgstr ""

#: ../contributors.rst:26
msgid ":abbr:`Holger Wansing holgerw (contributed and contributing since 2009)`,"
msgstr ""

#: ../contributors.rst:27
msgid ""
":abbr:`Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze "
"release)`,"
msgstr ""
":abbr:`Javier Fernández-Sanguino Peña jfs (llançament d'Etch, llançament "
"d'Squeeze)`,"

#: ../contributors.rst:28
msgid ":abbr:`Jens Seidel (German translation, innumerable contributions)`,"
msgstr ":abbr:`Jens Seidel (traducció a l'alemany, moltíssimes contribucions)`,"

#: ../contributors.rst:29
msgid ":abbr:`Jonas Meurer (syslog issues)`,"
msgstr ":abbr:`Jonas Meurer (problemes del syslog)`,"

#: ../contributors.rst:30
msgid ":abbr:`Jonathan Nieder (Squeeze release, Wheezy release)`,"
msgstr ""
":abbr:`Jonathan Nieder (llançament d'Squeeze, llançament de Wheezy)`,"

#: ../contributors.rst:31
msgid ":abbr:`Joost van Baal-Ilić joostvb (Wheezy release, Jessie release)`,"
msgstr ""
":abbr:`Joost van Baal-Ilić joostvb (llançament d'Wheezy, llançament de "
"Jessie)`,"

#: ../contributors.rst:32
msgid ":abbr:`Josip Rodin (previous releases)`,"
msgstr ":abbr:`Josip Rodin (llançaments anteriors)`,"

#: ../contributors.rst:33
msgid ":abbr:`Julien Cristau jcristau (Squeeze release, Wheezy release)`,"
msgstr ""
":abbr:`Julien Cristau jcristau (llançament d'Squeeze, llançament de "
"Wheezy)`,"

#: ../contributors.rst:34
msgid ":abbr:`Justin B Rye (English fixes)`,"
msgstr ":abbr:`Justin B Rye (correccions en l'anglès)`,"

#: ../contributors.rst:35
msgid ":abbr:`LaMont Jones (description of NFS issues)`,"
msgstr ":abbr:`LaMont Jones (descripció dels problemes de l'NFS)`,"

#: ../contributors.rst:36
msgid ":abbr:`Luk Claes (editors motivation manager)`,"
msgstr ":abbr:`Luk Claes (gestor de motivació dels editors)`,"

#: ../contributors.rst:37
msgid ":abbr:`Martin Michlmayr (ARM port issues)`,"
msgstr ":abbr:`Martin Michlmayr (problemes del port d'ARM)`,"

#: ../contributors.rst:38
msgid ":abbr:`Michael Biebl (syslog issues)`,"
msgstr ":abbr:`Michael Biebl (problemes del syslog)`,"

#: ../contributors.rst:39
msgid ":abbr:`Moritz Mühlenhoff (various contributions)`,"
msgstr ":abbr:`Moritz Mühlenhoff (contribucions diverses)`,"

#: ../contributors.rst:40
msgid ":abbr:`Niels Thykier nthykier (Jessie release)`,"
msgstr ":abbr:`Niels Thykier nthykier (llançament de Jessie)`,"

#: ../contributors.rst:41
msgid ":abbr:`Noah Meyerhans (innumerable contributions)`,"
msgstr ":abbr:`Noah Meyerhans (moltíssimes contribucions)`,"

#: ../contributors.rst:42
msgid ""
":abbr:`Noritada Kobayashi (Japanese translation (coordination), "
"innumerable contributions)`,"
msgstr ""
":abbr:`Noritada Kobayashi (traducció al japonès (coordinador), "
"moltíssimes contribucions)`,"

#: ../contributors.rst:43
msgid ":abbr:`Osamu Aoki (various contributions)`,"
msgstr ":abbr:`Osamu Aoki (contribucions diverses)`,"

#: ../contributors.rst:44
msgid ":abbr:`Paul Gevers elbrus (buster release)`,"
msgstr ":abbr:`Paul Gevers elbrus (llançament de Buster)`,"

#: ../contributors.rst:45
msgid ":abbr:`Peter Green (kernel version note)`,"
msgstr ":abbr:`Peter Green (nota de la versió del nucli)`,"

#: ../contributors.rst:46
msgid ":abbr:`Rob Bradford (Etch release)`,"
msgstr ":abbr:`Rob Bradford (llançament de Etch)`,"

#: ../contributors.rst:47
msgid ":abbr:`Samuel Thibault (description of d-i Braille support)`,"
msgstr ":abbr:`Samuel Thibault (descripció del suport de Braille al d-i)`,"

#: ../contributors.rst:48
msgid ":abbr:`Simon Bienlein (description of d-i Braille support)`,"
msgstr ":abbr:`Simon Bienlein (descripció del suport de Braille al d-i)`,"

#: ../contributors.rst:49
msgid ":abbr:`Simon Paillard spaillar-guest (innumerable contributions)`,"
msgstr ":abbr:`Simon Paillard spaillar-guest (moltíssimes contribucions)`,"

#: ../contributors.rst:50
msgid ":abbr:`Stefan Fritsch (description of Apache issues)`,"
msgstr ":abbr:`Stefan Fritsch (descripció dels problemes amb l'Apache)`,"

#: ../contributors.rst:51
msgid ":abbr:`Steve Langasek (Etch release)`,"
msgstr ":abbr:`Steve Langasek (llançament de Etch)`,"

#: ../contributors.rst:52
msgid ":abbr:`Steve McIntyre (Debian CDs)`,"
msgstr ""

#: ../contributors.rst:53
msgid ":abbr:`Tobias Scherer (description of \"proposed-update\")`,"
msgstr ":abbr:`Tobias Scherer (descripció de \"proposed-update\")`,"

#: ../contributors.rst:54
msgid ""
":abbr:`victory victory-guest (markup fixes, contributed and contributing "
"since 2006)`,"
msgstr ""

#: ../contributors.rst:55
msgid ":abbr:`Vincent McIntyre (description of \"proposed-update\")`,"
msgstr ":abbr:`Vincent McIntyre (descripció de \"proposed-update\")`,"

#: ../contributors.rst:56
msgid ":abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`."
msgstr ""
":abbr:`W. Martin Borgert (edició de les notes de llançament de Lenny, "
"migració a DocBook XML)`."

#: ../contributors.rst:58
msgid ""
"This document has been translated into many languages. Many thanks to all"
" the translators!"
msgstr "Este document s'ha traduït a molts idiomes. Moltes gràcies als traductors!"

#~ msgid ""
#~ ":abbr:`victory victory-guest victory.deb@gmail.com"
#~ " (markup fixes, contributed and "
#~ "contributing since 2006)`,"
#~ msgstr ""

